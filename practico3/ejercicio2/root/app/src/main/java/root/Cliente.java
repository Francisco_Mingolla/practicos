package root;

public class Cliente {
    // atributos
    private String nombre;
    private int dni;
    private String direccion;
    // constructor
    public Cliente(String nombre, int dni, String direccion){
        this.nombre=nombre;
        this.dni=dni;
        this.direccion=direccion;
    }
    public void mostrarCliente(){
        System.out.println("Nombre: " + nombre);
        System.out.println("Documento: " + dni);
        System.out.println("Direccion: " + direccion);
        
    }
}
