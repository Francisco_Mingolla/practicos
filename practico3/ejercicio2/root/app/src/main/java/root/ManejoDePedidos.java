package root;

import java.util.*;

public class ManejoDePedidos {
    // atributos
    Cliente cliente;
    Pedido pedido;
    Producto producto;
    ArrayList<Pedido> ListaPedidos;
    Scanner entrada;
    // constructor
    public ManejoDePedidos(ArrayList<Pedido> listapedidos){
        this.ListaPedidos=listapedidos;
    }
    //---------------------------------------------------------
    public void agregarPedido()
    {
        String nombre,direccion;
        int dni,numero,respuesta;
        boolean bucle=true;
        entrada = new Scanner(System.in);
        System.out.println("Complete la descripcion del Pedido");
        System.out.print("Nombre del cliente: ");
        nombre=entrada.nextLine();
        System.out.print("Documento: ");
        dni=entrada.nextInt();
        entrada.nextLine();
        System.out.print("Direccion: ");
        direccion=entrada.nextLine();
        System.out.print("Numero de pedido: ");
        numero=entrada.nextInt();
        cliente = new Cliente(nombre, dni, direccion);
        pedido = new Pedido(numero, cliente, "Pendiente");
        do {
            System.out.println("Desea agregar un prodcuto: ");
            System.out.println("1: Si       2: No");
            System.out.print("Respuesta numerica: ");
            respuesta=entrada.nextInt();
            switch (respuesta) {
                case 1:
                    producto=nuevoProducto();
                    pedido.agregarProducto(producto);
                    pedido.setPrecios(producto.getPrecioTotal());
                    break;
                case 2:
                    bucle=false;
                    break;
                default:
                    System.out.println("Respuesta incorrecta");
                    break;
            }
        } while (bucle);
        ListaPedidos.add(pedido);
        System.out.println("Pedido agregado");
    }
    //---------------------------------------------------------
    public Producto nuevoProducto()
    {
        String nombre;
        int cantidad;
        double precio;
        int impuesto;
        entrada = new Scanner(System.in);
        System.out.println("Complete la descripcion del producto");
        System.out.print("Nombre: ");
        nombre=entrada.nextLine();
        System.out.print("Cantidad: ");
        cantidad=entrada.nextInt();
        System.out.print("Precio x unidad: ");
        precio=entrada.nextDouble();
        producto = new Producto(nombre, cantidad, precio);
        System.out.println("Impuestos: ");
        System.out.println("1: Nacional");
        System.out.println("2: Extranjero");
        System.out.print("Tipo de Impuesto: ");
        impuesto=entrada.nextInt();
        if((impuesto==1)||(impuesto==2))
            producto.cargarImpuesto(impuesto);
        else
            System.out.println("Impuesto no valido");
        return producto;
    }
    //---------------------------------------------------------
    public void mostrar_pedidos()
    {
        for (Pedido pedido : ListaPedidos)
        {
            System.out.println("-------------------------------------------------------");
            pedido.mostrarPedido();
        }
    }
    //---------------------------------------------------------
    public void cambiarEstado()
    {
        int respuesta,respuesta2;
        double montoPago;
        boolean comprobarPedido = false, comprobarEstado = false;
        entrada = new Scanner(System.in);
        System.out.println("");
        System.out.println("Pedidos");
        mostrar_pedidos();
        System.out.println("");
        System.out.print("Numero del pedido a cambiar: ");
        respuesta = entrada.nextInt();
        for (Pedido pedido : ListaPedidos)
        {
            if(respuesta == pedido.getNumero())
            {
                System.out.println("El pedido con numero: " + respuesta + " y estado -" + pedido.getEstadoPedido() + "- Cambiara su estado");
                menu_estados();
                respuesta = entrada.nextInt();
                switch (respuesta)
                {
                    case 1:
                        System.out.println("Ingrese el tipo de pago: ");
                        System.out.println("1: Efectivo");
                        System.out.println("2: Tarjeta");
                        System.out.println("3: Cheque");
                        System.out.print("Respuesta numerica: ");
                        respuesta2=entrada.nextInt();
                        switch (respuesta2) {
                            case 1:
                                System.out.println("Total del pedido: " + pedido.getPrecioTotal());
                                System.out.print("Ingrese el monto a pagar: ");
                                montoPago=entrada.nextDouble();
                                if(montoPago==pedido.getPrecioTotal()){
                                    pedido.setEstadoPedido("Pagado");
                                    pedido.setMetodoPago("Efectivo");
                                }
                                else
                                    System.out.println("El monto ingresado no coincide con el monto del pedido");
                                break;
                            case 2:
                                System.out.println("Total del pedido: " + pedido.getPrecioTotal());
                                System.out.print("Ingrese el monto a pagar: ");
                                montoPago=entrada.nextDouble();
                                if(montoPago==pedido.getPrecioTotal()){
                                    pedido.setEstadoPedido("Pagado");
                                    pedido.setMetodoPago("Tarjeta");
                                }
                                else
                                    System.out.println("El monto ingresado no coincide con el monto del pedido");
                                break;
                            case 3:
                                System.out.println("Total del pedido: " + pedido.getPrecioTotal());
                                System.out.print("Ingrese el monto a pagar: ");
                                montoPago=entrada.nextDouble();
                                if(montoPago==pedido.getPrecioTotal()){
                                    pedido.setEstadoPedido("Pagado");
                                    pedido.setMetodoPago("Cheque");
                                }
                                else
                                    System.out.println("El monto ingresado no coincide con el monto del pedido");
                                break;
                            default:
                                break;
                        }
                        comprobarEstado = true;
                        break;
                    case 2:
                        pedido.setEstadoPedido("Procesando");
                        comprobarEstado = true;
                        break;
                    case 3:
                        pedido.setEstadoPedido("Enviado");
                        comprobarEstado = true;
                        break;
                    case 4:
                        pedido.setEstadoPedido("Entregado");
                        comprobarEstado = true;
                    default:
                        System.out.println("");
                        System.out.println("Respuesta incorrecta");
                        break;
                }
                comprobarPedido = true;
            }
        }
        if(comprobarPedido)
        {
            if(comprobarEstado)
            {
                System.out.println("");
                System.out.println("Estado del pedido actualizado");
            }
            else{
                System.out.println("Pedido encontrado, pero no pudo ser actualizado");
            }
        }
        else
        {
            System.out.println("");
            System.out.println("Pedido no encontrado");
        }
    }
    public void menu_estados(){
        System.out.println("----------------------- Estados del pedido -----------------------");
        System.out.println("1: Pagado");
        System.out.println("2: Procesando");
        System.out.println("3: Enviado");
        System.out.println("4: Entregado");
        System.out.print("Respuesta numerica: ");
    }
}
