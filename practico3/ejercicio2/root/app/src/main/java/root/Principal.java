package root;
import java.io.IOException;
import java.util.*;

public class Principal 
{
    public static void main(String[] args) throws IOException
    {
        int respuesta;
        ArrayList<Pedido> listaPedidos = new ArrayList<Pedido>();
        Scanner entrada = new Scanner(System.in);
        ManejoDePedidos manejoDepedidos = new ManejoDePedidos(listaPedidos);
        do
        {
            menu();
            respuesta=entrada.nextInt();
            switch (respuesta)
            {
                case 1:
                    manejoDepedidos.agregarPedido();
                    break;
                case 2:
                    if(listaPedidos.size()>0)
                    {
                        manejoDepedidos.mostrar_pedidos();
                    }
                    else
                    {
                        System.out.println("");
                        System.out.println("No se encontraron pedidos");
                    }
                    break;
                case 3:
                    if(listaPedidos.size()>0)
                    {
                        manejoDepedidos.cambiarEstado();
                    }
                    else
                    {
                        System.out.println("");
                        System.out.println("No se encontraron pedidos");
                    }
                    break;
                case 4:
                    break;
                default:
                    System.out.println("Respuesta incorrecta");
                    break;
            }
        }while(respuesta!=4);
        System.out.println("Salio del algoritmo");
    }
    //Metodos
    private static void menu()
    {
        System.out.println("");
        System.out.println("Sistema de manejos de 'Pedidos'");
        System.out.println("Menu:");
        System.out.println("1: Agregar");
        System.out.println("2: Mostrar pedidos");
        System.out.println("3: Cambiar el estado del pedido");
        System.out.println("4: Salir");
        System.out.print("Respuesta numerica: ");
    }

}