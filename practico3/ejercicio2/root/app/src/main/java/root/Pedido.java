package root;
import java.util.ArrayList;

public class Pedido {
    // atributos
    private int numero;
    Cliente cliente;
    private String estadoPedido;
    Producto producto;
    ArrayList<Producto> listaProductos;
    private double precioTotal;
    private String metodoPago;
    // constructor
    public Pedido(int numero, Cliente cliente, String estadoPedido){
        this.numero = numero;
        this.cliente = cliente;
        this.listaProductos = new ArrayList<Producto>();
        this.estadoPedido = estadoPedido;
        this.metodoPago="No pagado";
    }
    // -----------------------------------------------------------------------
    public void setEstadoPedido(String estado)
    {
        this.estadoPedido=estado;
    }
    public void agregarProducto(Producto producto)
    {
        this.listaProductos.add(producto);
    }
    public void setPrecios(double precio)
    {
        this.precioTotal+=precio;
    }
    public void setMetodoPago(String metodoPago){
        this.metodoPago=metodoPago;
    }
    public int getNumero()
    {
        return numero;
    }
    public String getEstadoPedido()
    {
        return estadoPedido;
    }
    public double getPrecioTotal()
    {
        return precioTotal;
    }
    public void mostrarPedido(){
        System.out.println("");
        System.out.println("Informacion del cliente: ");
        cliente.mostrarCliente();
        System.out.println("Informacion del pedido: ");
        System.out.println("Numero del pedido: " + numero);
        System.out.println("Estado del pedido: " + estadoPedido);
        System.out.println("Metodo de pago: " + metodoPago);
        System.out.println("Lista de Productos:");
        for (Producto producto : listaProductos)
        {
            producto.mostrarProducto();            
        }
        System.out.println(" ****** Precio total del pedido: " + precioTotal + " ****** ");
    }
}
