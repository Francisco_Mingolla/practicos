package root;

public class Producto {
    // atributos
    private String nombre;
    private int cantidad;
    private double precioUnidad;
    private double precioTotal;
    private double impuestoNacional=0.2;
    private double impuestoExterior=1.005;
    public Producto(String nombre, int cantidad, double precioUnidad){
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.precioUnidad = precioUnidad;
        this.precioTotal = cantidad * precioUnidad;
    }
    public void mostrarProducto(){
        System.out.println("Nombre: " + nombre);
        System.out.println("Cantidad: " + cantidad);
        System.out.println("Precio x unidad : " + precioUnidad + " Precio Total : " + precioTotal);
    }
    public double getPrecioTotal(){
        return precioTotal;
    }
    public void cargarImpuesto(int impuesto)
    {
        switch (impuesto) {
            case 1:
                precioTotal+=precioTotal*impuestoNacional;
                break;
            case 2:
                precioTotal+=precioTotal*impuestoExterior;
                break;
            default:
                break;
        }
    }
}
