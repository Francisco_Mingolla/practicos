package root;

import java.util.ArrayList;
import java.util.Scanner;

// falta revisar

public class Liquidaciones {
    private Scanner entrada;
    private Contrataciones contrataciones;
    // constructor
    public Liquidaciones(Contrataciones contrataciones){
        this.contrataciones=contrataciones;
    }
    // comportamientos
    public void pagarEmpleado() throws EmpleadoInexistenteException
    {
        Empleado empleadoEncontrado=null;
        int respuesta;
        Integer dni;
        entrada = new Scanner(System.in);
        System.out.print("Documento del empleado : ");
        dni=entrada.nextInt();
        empleadoEncontrado=contrataciones.buscarEmpleadoDni(dni);
        if(empleadoEncontrado==null)
            throw new EmpleadoInexistenteException();
        else
        {
            empleadoEncontrado.mostrarDatos();
            System.out.println("Desea cambiar el estado de " +empleadoEncontrado.getEstadoCobro());
            System.out.println("1: Si");
            System.out.println("2: No");
            System.out.print("Respuesta numerica: ");
            respuesta=entrada.nextInt();
            switch (respuesta) {
                case 1:
                    empleadoEncontrado=cambiarEstadoEmpleado(empleadoEncontrado, empleadoEncontrado.getEstado());
                    break;
                case 2:
                    break;
                default:
                    System.out.println("Respuesta incorrecta");
                    break;
            }
        }   
    }
    public Empleado cambiarEstadoEmpleado(Empleado empleado, boolean estadoDeCobro){
        if(estadoDeCobro)
            empleado.setEstado(false);
        else
            empleado.setEstado(true);
        return empleado;
    }


    public void pagarServicio() throws ServicioInexistenteException
    {
        Servicio servicioEncontrado=null;
        int respuesta;
        Integer cuit;
        entrada = new Scanner(System.in);
        System.out.print("Cuit del servicio : ");
        cuit=entrada.nextInt();
        // -----------------------------------------------------------
        entrada = new Scanner(System.in);
        System.out.print("Cuit del servicio : ");
        cuit=entrada.nextInt();
        servicioEncontrado=contrataciones.buscarServicioCuit(cuit);
        if(servicioEncontrado==null)
            throw new ServicioInexistenteException();
        else
        {
            servicioEncontrado.mostrarDatos();
            System.out.println("Desea cambiar el estado de " +servicioEncontrado.getEstado());
            System.out.println("1: Si");
            System.out.println("2: No");
            System.out.print("Respuesta numerica: ");
            respuesta=entrada.nextInt();
            switch (respuesta) {
                case 1:
                    servicioEncontrado=cambiarEstadoServicio(servicioEncontrado, servicioEncontrado.getEstado());
                    break;
                case 2:
                    break;
                default:
                    System.out.println("Respuesta incorrecta");
                    break;
            }
        }   
    }
    public Servicio cambiarEstadoServicio(Servicio servicio, boolean estadoDePago){
        if(estadoDePago)
            servicio.setEstado(false);
        else
            servicio.setEstado(true);
        return servicio;
    }
    // metodos para reinicair los montos
}
