package root;

// falta revisar

public class EmpleadoComisionado extends Empleado{
    // atributos
    private double salarioComisionado;
    private int cantidadVentas;
    private double bonificacion;
    private double sueldo;
    // constructor
    public EmpleadoComisionado(String nombre, Integer dni,String tipoEmpleado, boolean estadoDeCobro, double salarioComisionado, int cantidadVentas, double bonificacion) {
        super(nombre, dni, tipoEmpleado, estadoDeCobro);
        this.salarioComisionado=salarioComisionado;
        this.cantidadVentas=cantidadVentas;
        this.bonificacion=bonificacion;
    }
    // set
    public void setCantVentas(int ventas)
    {
        this.cantidadVentas=ventas;
    }
    // get
    public int getCantVentas(){
        return cantidadVentas;
    }
    public double getSalarioComision(){
        return salarioComisionado;
    }
    public double getMonto(){
        calcularSueldo();
        return sueldo;
    }
    // comportamientos
    public void calcularSueldo(){
        if((cantidadVentas!=0)&&(bonificacion!=0))
            this.sueldo=(salarioComisionado*cantidadVentas)*bonificacion;
        else
            this.sueldo=salarioComisionado;
    }
}
