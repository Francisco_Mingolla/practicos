package root;

public class ServicioExistenteExcepcion extends RuntimeException{
    public ServicioExistenteExcepcion(){
        super("EL servicio que quiere agregar ya existente");
    }
}
