package root;

// falta revisar

public class EmpleadoAsalariado extends Empleado{
    // atributos
    private double salarioBase;
    private double sueldo;
    // constructor
    public EmpleadoAsalariado(String nombre, Integer dni,String tipoEmpleado,boolean estadoDeCobro, double salarioBase) {
        super(nombre, dni, tipoEmpleado, estadoDeCobro);
        this.salarioBase=salarioBase;
    }
    // comportamientos
    public void calcularSueldo(){
        this.sueldo=salarioBase;
    }
    // get
    public double getSalarioBase(){
        return salarioBase;
    }
    public double getMonto(){
        calcularSueldo();
        return sueldo;
    }
}
