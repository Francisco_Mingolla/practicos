package root;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.ArrayList;

public class LiquidacionesTest {
    private ArrayList<Empleado> lista_Empleados;
    private ArrayList<Servicio> lista_Servicios;
    private Contrataciones contrataciones;
    private Liquidaciones liquidaciones;
    private Empleado empleado;
    private Servicio servicio;
    @Test public void verificarCambiarEstadoEmpelado() {
        boolean estadoActual=false;
        boolean estadoNuevo=true;
        lista_Empleados = new ArrayList<>();
        lista_Servicios = new ArrayList<>();
        contrataciones = new Contrataciones(lista_Empleados, lista_Servicios);
        liquidaciones = new Liquidaciones(contrataciones);
        empleado = contrataciones.setEmpleadoAsalariado("francisco", 42843636, "Asalariado", estadoActual, 564564); 
        empleado = liquidaciones.cambiarEstadoEmpleado(empleado, estadoActual);
        assertEquals(estadoNuevo, empleado.getEstado());
    }
    @Test public void verificarCambiarEstadoServicio() {
        boolean estadoActual=false;
        boolean estadoNuevo=true;
        lista_Empleados = new ArrayList<>();
        lista_Servicios = new ArrayList<>();
        contrataciones = new Contrataciones(lista_Empleados, lista_Servicios);
        liquidaciones = new Liquidaciones(contrataciones);
        servicio = contrataciones.setServicio("luz", 42843636, 456);
        empleado = contrataciones.setEmpleadoAsalariado("francisco", 42843636, "Asalariado", estadoActual, 56561); 
        empleado = liquidaciones.cambiarEstadoEmpleado(empleado, estadoActual);
        assertEquals(estadoNuevo, empleado.getEstado());
    }
}
