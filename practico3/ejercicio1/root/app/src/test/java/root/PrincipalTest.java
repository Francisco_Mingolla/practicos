package root;


import org.junit.Test;
import static org.junit.Assert.*;
public class PrincipalTest
 {
    @Test public void appHasAGreeting() {
        Principal classUnderTest = new Principal();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
}