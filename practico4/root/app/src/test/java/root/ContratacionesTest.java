package root;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.ArrayList;

public class ContratacionesTest {

    private Contrataciones contrataciones;
    private ArrayList<Empleado> lista_Empleados;
    private ArrayList<Servicio> lista_Servicios;
    private Empleado empleado;
    private Servicio servicio;
    @Test public void verificarComprobarSueldosyBonificacion() {
        double salarioBase=150, salarioComisionado=100, bonificacion=1.5;
        lista_Empleados = new ArrayList<>();
        lista_Servicios = new ArrayList<>();
        contrataciones = new Contrataciones(lista_Empleados, lista_Servicios);
        contrataciones.setSalarioBase(salarioBase);
        contrataciones.setSalarioComision(salarioComisionado);
        contrataciones.setBonificacion(bonificacion);
        assertEquals(true, contrataciones.comprobarSueldos());
    }
    @Test public void verificarSetEmpleadoAsalariado(){
        lista_Empleados = new ArrayList<>();
        lista_Servicios = new ArrayList<>();
        contrataciones = new Contrataciones(lista_Empleados, lista_Servicios);
        Double salarioBase=1500.00;
        Empleado comprobaEmpleado = new EmpleadoAsalariado("francisco", 42843636, "Asalariado", false, salarioBase);
        empleado = contrataciones.setEmpleadoAsalariado("francisco", 42843636, "Asalariado", false, salarioBase); 
        comprobaEmpleado.equals(empleado);
    }
    @Test public void verificarSetEmpleadoComisionado(){
        lista_Empleados = new ArrayList<>();
        lista_Servicios = new ArrayList<>();
        contrataciones = new Contrataciones(lista_Empleados, lista_Servicios);
        Double salarioComisionado=1500.00;
        Double bonificacion=1.5;
        Empleado comprobaEmpleado = new EmpleadoComisionado("marcos", 42843636, "Asalariado", false, salarioComisionado, 0, bonificacion);
        empleado = contrataciones.setEmpleadoComisionado("marcos", 42843636, "Asalariado", false, salarioComisionado, 0, bonificacion); 
        comprobaEmpleado.equals(empleado);
    }
    @Test public void verificarSetListaEmpleados(){
        lista_Empleados = new ArrayList<>();
        lista_Servicios = new ArrayList<>();
        Double salarioBase=1500.00;
        Double bonificacion=1.5;
        contrataciones = new Contrataciones(lista_Empleados, lista_Servicios);
        contrataciones.setEmpleadoAsalariado("francisco", 42843636, "Asalariado", false, salarioBase);
        contrataciones.setEmpleadoComisionado("marcos", 4284366, "Asalariado", false, salarioBase, 0, bonificacion);
        assertEquals(2, contrataciones.getListaEmpleados().size());
    }
    @Test public void verificarSetServicio(){
        lista_Empleados = new ArrayList<>();
        lista_Servicios = new ArrayList<>();
        contrataciones = new Contrataciones(lista_Empleados, lista_Servicios);
        Servicio comprobaServicio = new Servicio("luz", 1563, 500);
        servicio = contrataciones.setServicio("luz", 1563, 500);
        comprobaServicio.equals(servicio);
    }
    @Test public void verificarSetListaServicios(){
        lista_Empleados = new ArrayList<>();
        lista_Servicios = new ArrayList<>();
        contrataciones = new Contrataciones(lista_Empleados, lista_Servicios);
        contrataciones.setServicio("agua", 4564, 0);
        contrataciones.setServicio("luz", 454, 0);
        assertEquals(2, contrataciones.getListaServicio().size());
    }
    @Test public void verificarBuscarEmpleado(){
        lista_Empleados = new ArrayList<>();
        lista_Servicios = new ArrayList<>();
        contrataciones = new Contrataciones(lista_Empleados, lista_Servicios);
        Integer dni = 1;
        Double salarioBase=1500.00;
        Empleado comproEmpleado;
        empleado = contrataciones.setEmpleadoAsalariado("francisco", dni, "Asalariado", false, salarioBase);
        comproEmpleado=contrataciones.buscarEmpleadoDni(dni);
        comproEmpleado.equals(empleado);
    }
    @Test public void verificarEliminarEmpleado(){
        lista_Empleados = new ArrayList<>();
        lista_Servicios = new ArrayList<>();
        contrataciones = new Contrataciones(lista_Empleados, lista_Servicios);
        Integer dni = 1;
        Double salarioBase=1500.00;
        contrataciones.setEmpleadoAsalariado("francisco", dni, "Asalariado", false, salarioBase);
        contrataciones.eliminarEmpleado(dni);
        assertEquals(0, contrataciones.getListaEmpleados().size());
    }
    @Test public void verificarBuscarServicio(){
        lista_Empleados = new ArrayList<>();
        lista_Servicios = new ArrayList<>();
        contrataciones = new Contrataciones(lista_Empleados, lista_Servicios);
        Integer cuit = 1;
        Servicio comproServicio;
        servicio = contrataciones.setServicio("agua", cuit, 0);
        comproServicio=contrataciones.buscarServicioCuit(cuit);
        comproServicio.equals(servicio);
    }
    @Test public void verificarEliminarServicio(){
        lista_Empleados = new ArrayList<>();
        lista_Servicios = new ArrayList<>();
        contrataciones = new Contrataciones(lista_Empleados, lista_Servicios);
        Integer cuit = 1;
        contrataciones.setServicio("agua", cuit, 0);
        contrataciones.eliminarServicio(cuit);
        assertEquals(0, contrataciones.getListaServicio().size());
    }
}
 