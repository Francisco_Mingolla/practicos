package root;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class EmpleadoComisiondadoTest {
    @Test public void verificarSueldoEmpleadoComisionado(){
        Double sueldo=1.500;
        Integer ventas=2;
        Double bonificacion=1.5;
        Double total=(sueldo*ventas)*bonificacion;
        EmpleadoComisionado empleado = new EmpleadoComisionado("marcos", 42843636, "Asalariado", false, sueldo, 0, bonificacion);
        empleado.setCantVentas(ventas);
        assertEquals(total, empleado.getMonto());
    }
}
