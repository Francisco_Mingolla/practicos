package root;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.ArrayList;

public class LiquidacionesTest {
    private ArrayList<Empleado> lista_Empleados;
    private ArrayList<Servicio> lista_Servicios;
    private Contrataciones contrataciones;
    private Liquidaciones liquidaciones;
    private Empleado empleado;
    private Servicio servicio;
    private Double salarioBase=1500.00;
    private Double salarioComisionado=750.00;
    private Double bonificacion=1.5;
    @Test public void verificarCambiarEstadoEmpleado() {
        boolean estadoActual=false;
        boolean estadoNuevo=true;
        lista_Empleados = new ArrayList<>();
        lista_Servicios = new ArrayList<>();
        contrataciones = new Contrataciones(lista_Empleados, lista_Servicios);
        liquidaciones = new Liquidaciones(contrataciones);
        empleado = contrataciones.setEmpleadoAsalariado("francisco", 42843636, "Asalariado", estadoActual, salarioBase); 
        empleado = liquidaciones.cambiarEstadoEmpleado(empleado, estadoActual);
        assertEquals(estadoNuevo, empleado.getEstado());
    }
    @Test public void verificarCambiarEstadoServicio() {
        boolean estadoActual=false;
        boolean estadoNuevo=true;
        lista_Empleados = new ArrayList<>();
        lista_Servicios = new ArrayList<>();
        contrataciones = new Contrataciones(lista_Empleados, lista_Servicios);
        liquidaciones = new Liquidaciones(contrataciones);
        servicio = contrataciones.setServicio("luz", 42843636, 456);
        servicio = liquidaciones.cambiarEstadoServicio(servicio, estadoActual);
        assertEquals(estadoNuevo, servicio.getEstado());
    }
    @Test public void verificarSetVentasComisionado() {
        lista_Empleados = new ArrayList<>();
        lista_Servicios = new ArrayList<>();
        contrataciones = new Contrataciones(lista_Empleados, lista_Servicios);
        liquidaciones = new Liquidaciones(contrataciones);
        Integer ventas=2;
        Double sueldoComisionado=(salarioComisionado*ventas)*bonificacion;
        EmpleadoComisionado empleado = (EmpleadoComisionado)contrataciones.setEmpleadoComisionado("marcos", 42843637, "Asalariado", false, salarioComisionado, 0, bonificacion); 
        liquidaciones.setVentasComisionado(empleado, ventas);
        assertEquals(ventas, empleado.getCantVentas());
        assertEquals(sueldoComisionado, empleado.getMonto());
    }
    @Test public void verificarGetListaComisionados() {
        lista_Empleados = new ArrayList<>();
        lista_Servicios = new ArrayList<>();
        contrataciones = new Contrataciones(lista_Empleados, lista_Servicios);
        liquidaciones = new Liquidaciones(contrataciones);
        empleado = contrataciones.setEmpleadoComisionado("marcos", 42843637, "Asalariado", false, salarioComisionado, 0, bonificacion); 
        assertEquals(1, liquidaciones.getListaComisionados().size());
    }
}
