package root;

import java.util.ArrayList;
import java.util.Scanner;

// falta revisar

public class Contrataciones {
    // atributos
    private Scanner entrada;
    private Empleado empleado;
    private Servicio servicio;
    private ArrayList<Empleado> lista_Empleados;
    private ArrayList<Servicio> lista_Servicios;
    private Double salarioBase=0.0;
    private Double salarioComisionado=0.0;
    private Double bonificacion=0.0;
    // constructor
    public Contrataciones(ArrayList<Empleado> lista_Empleados, ArrayList<Servicio> lista_Servicios){
        this.lista_Empleados=lista_Empleados;
        this.lista_Servicios=lista_Servicios;
    }
    // seccion sueldos
    public void cargarSueldos(){
        Double salarioB, salarioC, bonificacion;
        entrada = new Scanner(System.in);
        System.out.println("Complete la informacion del salario de los empleados");
        System.out.print("Sueldo del empleado Asalariado: ");
        salarioB=entrada.nextDouble();
        setSalarioBase(salarioB);
        System.out.print("Sueldo del empleado Comisionado: ");
        salarioC=entrada.nextDouble();
        setSalarioComision(salarioC);
        System.out.print("Bonificacion para los empleado comisionados?:  ");
        bonificacion=entrada.nextDouble();
        setBonificacion(bonificacion);
    }
    //
    public void setSalarioBase(Double salarioBase){
        this.salarioBase=salarioBase;
    }
    public void setSalarioComision(Double salarioComisionado){
        this.salarioComisionado=salarioComisionado;
    }
    public void setBonificacion(Double bonificacion){
        this.bonificacion=bonificacion;
    }
    public Double getSalarioBase(){
        return salarioBase;
    }
    public Double getSalarioComisionado(){
        return salarioComisionado;
    }
    public Double getBonificacion(){
        return bonificacion;
    }
    // test
    public boolean comprobarSueldos(){
        boolean resultado;
        if((salarioBase!=0)&&(salarioComisionado!=0)&&(bonificacion!=0))
            resultado=true;
        else
            resultado=false;
        return resultado;
    }
    public void mostrarSueldosyBonificacion(){
        System.out.println("Sueldos: ");
        System.out.println("Sueldo del empleado Asalariado: " + getSalarioBase());
        System.out.println("Sueldo del empleado Comisionado: " + getSalarioComisionado());
        System.out.println("Bonificacion de los empleados comisionados: "+ getBonificacion());
    }
    // seccion empleados
    public void cargarEmpleado() throws EmpleadoExistenteExcepcion{
        String nombre,tipoEmpleado;
        Integer dni;
        boolean estadoDeCobro=false;
        boolean bucle=true;
        int respuesta;
        entrada = new Scanner(System.in);
        System.out.println("Complete la informacion del empleado:");
        System.out.print("Nombre:");
        nombre=entrada.nextLine();
        System.out.print("Documento:");
        dni=entrada.nextInt();
        do{
            System.out.println("Ingrese el tipo de Empleado en forma numerica");
            System.out.println("1: Empleado Asalariado");
            System.out.println("2: Empleado Comisionado");
            System.out.print("Respuesta: ");
            respuesta=entrada.nextInt();
            switch (respuesta) {
                case 1:
                    tipoEmpleado="Asalariado";
                    empleado = setEmpleadoAsalariado(nombre, dni, tipoEmpleado, estadoDeCobro, salarioBase);
                    bucle=false;
                    break;
                case 2:
                    tipoEmpleado="Comisionado";
                    empleado = setEmpleadoComisionado(nombre, dni, tipoEmpleado, estadoDeCobro, salarioComisionado,0,bonificacion);
                    bucle=false;
                    break;
                default:
                    System.out.println("Opcion incorrecta");
                    break;
            }
        }while(bucle);
    }
    // creo empleados
    public Empleado setEmpleadoAsalariado(String nombre, Integer dni, String tipoEmpleado, boolean estadoDeCobro, Double salarioBase) throws EmpleadoExistenteExcepcion{
        empleado = new EmpleadoAsalariado(nombre, dni, tipoEmpleado, estadoDeCobro, salarioBase);
        setListaEmpleados(empleado);
        return empleado;
    }
    public Empleado setEmpleadoComisionado(String nombre, Integer dni, String tipoEmpleado, boolean estadoDeCobro, Double salarioComisionado, Integer cantidadVentas, Double bonificacion) throws EmpleadoExistenteExcepcion{
        empleado = new EmpleadoComisionado(nombre, dni, tipoEmpleado, estadoDeCobro, salarioComisionado, cantidadVentas, bonificacion);
        setListaEmpleados(empleado);
        return empleado;
    }
    // agrego empleados
    public void setListaEmpleados(Empleado empleadoNuevo) throws EmpleadoExistenteExcepcion {
        for (Empleado empleado : lista_Empleados) {
            if(empleado.equals(empleadoNuevo))
                throw new EmpleadoExistenteExcepcion();
        }
        this.lista_Empleados.add(empleadoNuevo);
    }
    // devuelvo lista de empleados
    public ArrayList<Empleado> getListaEmpleados(){
        return lista_Empleados;
    }
    // muestro lista empleados
    public void mostrarEmpleados(){
        System.out.println("Lista de Empleados: ");
        for (Empleado empleado : lista_Empleados) {
            empleado.mostrarDatos();
            System.out.println("");
        }
    }
    // seccion servicio
    public void cargarServicio() throws ServicioExistenteExcepcion{
        String nombre;
        Integer cuit;
        Double monto;
        entrada = new Scanner(System.in);
        System.out.println("Complete la informacion del Servicio:");
        System.out.print("Razon social - Nombre : ");
        nombre = entrada.nextLine();
        System.out.print("Cuit - solo numeros: ");
        cuit = entrada.nextInt();
        System.out.print("Monto del servicio: ");
        monto = entrada.nextDouble();
        setServicio(nombre, cuit, monto);
    }
    // creo servicio
    public Servicio setServicio(String nombre, Integer cuit, double monto) throws ServicioExistenteExcepcion
    {
        servicio = new Servicio(nombre, cuit, monto);
        setListaServicios(servicio);
        return servicio;
    }
    // agerego servicios
    public void setListaServicios(Servicio servicioNuevo) throws ServicioExistenteExcepcion
    {
        for (Servicio servicio : lista_Servicios) {
            if (servicio.equals(servicioNuevo))
                throw new ServicioExistenteExcepcion();
        }
        this.lista_Servicios.add(servicioNuevo);
    }
    // devuelvo lista de servicios
    public ArrayList<Servicio> getListaServicio(){
        return lista_Servicios;
    }
    // muestro servicios
    public void mostrarServicios(){
        System.out.println("Lista de Servicios: ");
        for (Servicio servicio : lista_Servicios) {
            servicio.mostrarDatos();
            System.out.println("");
        }
    }
    // buscar empleado segun el dni
    public Empleado buscarEmpleadoDni(Integer dniBuscado)  throws EmpleadoInexistenteException{
        Empleado empleadoEncontrado = null;
        for (Empleado empleado : lista_Empleados) {
            if(empleado.getDni().equals(dniBuscado)){
                empleadoEncontrado=empleado;
                break;
            }
        }
        if(empleadoEncontrado == null){
            throw new EmpleadoInexistenteException();
        }
        return empleadoEncontrado;
    }
    //eliminar un empleado
    public void eliminarEmpleado(Integer dniBuscado)  throws EmpleadoInexistenteException{
        Empleado empleadoEncontrado = buscarEmpleadoDni(dniBuscado);
        this.lista_Empleados.remove(empleadoEncontrado);
    }
    //buscar servicio segun el cuit
    public Servicio buscarServicioCuit(Integer cuitBuscado)  throws ServicioInexistenteException{
        Servicio servicioEncontrado = null;
        for (Servicio servicio : lista_Servicios) {
            if(servicio.getCuit().equals(cuitBuscado)){
                servicioEncontrado=servicio;
                break;
            }
        }
        if(servicioEncontrado == null){
            throw new ServicioInexistenteException();
        }
        return servicioEncontrado;
    }
    //eliminar un servicio
    public void eliminarServicio(Integer cuitBuscado)  throws ServicioInexistenteException{
        Servicio servicioEncontrado = buscarServicioCuit(cuitBuscado);
        this.lista_Servicios.remove(servicioEncontrado);
    }
    // eliminar empleado
    public void buscar_eliminarEmpleado() throws EmpleadoInexistenteException{
        Empleado empleadoEncontrado=null;
        int respuesta;
        Integer dni;
        entrada = new Scanner(System.in);
        System.out.print("Documento del empleado : ");
        dni=entrada.nextInt();
        empleadoEncontrado=buscarEmpleadoDni(dni);
        if(empleadoEncontrado!=null)
        {
            empleadoEncontrado.mostrarDatos();
            System.out.println("Desea eliminar al empleado " + empleadoEncontrado.getNombre()+" "+empleadoEncontrado.getDni());
            System.out.println("1: Si");
            System.out.println("2: No");
            System.out.print("Respuesta numerica: ");
            respuesta=entrada.nextInt();
            switch (respuesta) {
                case 1:
                    eliminarEmpleado(dni);
                    System.out.println("Empleado eliminado");
                    break;
                case 2:
                    break;
                default:
                    System.out.println("Respuesta incorrecta");
                    break;
            }
        }  
    }
    // eliminar servicio
    public void buscar_eliminarServicio() throws ServicioInexistenteException{
        Servicio servicioEncontrado=null;
        int respuesta;
        Integer cuit;
        entrada = new Scanner(System.in);
        System.out.print("Cuit del Servicio : ");
        cuit=entrada.nextInt();
        servicioEncontrado=buscarServicioCuit(cuit);
        if(servicioEncontrado!=null)
        {
            servicioEncontrado.mostrarDatos();
            System.out.println("Desea eliminar el servicio " + servicioEncontrado.getNombre()+" "+servicioEncontrado.getCuit());
            System.out.println("1: Si");
            System.out.println("2: No");
            System.out.print("Respuesta numerica: ");
            respuesta=entrada.nextInt();
            switch (respuesta) {
                case 1:
                    eliminarServicio(cuit);
                    System.out.println("Servicio eliminado");
                    break;
                case 2:
                    break;
                default:
                    System.out.println("Respuesta incorrecta");
                    break;
            }
        }  
    }
}
