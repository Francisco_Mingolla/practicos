package root;

public class EmpleadoInexistenteException extends RuntimeException{
    public EmpleadoInexistenteException(){
        super("El empleado no existe");
    }
}
