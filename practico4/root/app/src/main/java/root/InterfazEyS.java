package root;

public interface InterfazEyS {
    public abstract boolean getEstado();
    public abstract void setEstado(boolean estado_de_Cobro);
    public abstract Double getMonto();
    public abstract void mostrarDatos();
    public abstract String getEstadoCobro();
}
