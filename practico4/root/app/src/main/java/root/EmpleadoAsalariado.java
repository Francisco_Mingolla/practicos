package root;

public class EmpleadoAsalariado extends Empleado{
    // atributos
    private Double salarioBase;
    private Double sueldo;
    // constructor
    public EmpleadoAsalariado(String nombre, Integer dni,String tipoEmpleado,boolean estadoDeCobro, Double salarioBase) {
        super(nombre, dni, tipoEmpleado, estadoDeCobro);
        this.salarioBase=salarioBase;
    }
    // comportamientos
    public Double calcularSueldo(){
        this.sueldo=salarioBase;
        return sueldo;
    }
    // get
    public Double getSalarioBase(){
        return salarioBase;
    }
    public Double getMonto(){
        return calcularSueldo();
    }
}
