package root;

public class ServicioInexistenteException extends RuntimeException{
    public ServicioInexistenteException(){
        super("El servicio no existe");
    }
}
