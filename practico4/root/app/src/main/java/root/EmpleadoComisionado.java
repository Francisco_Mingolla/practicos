package root;

public class EmpleadoComisionado extends Empleado{
    // atributos
    private Double salarioComisionado;
    private Integer cantidadVentas;
    private Double bonificacion;
    private Double sueldo;
    // constructor
    public EmpleadoComisionado(String nombre, Integer dni, String tipoEmpleado, boolean estadoDeCobro, Double salarioComisionado, Integer cantidadVentas, Double bonificacion) {
        super(nombre, dni, tipoEmpleado, estadoDeCobro);
        this.salarioComisionado=salarioComisionado;
        this.cantidadVentas=cantidadVentas;
        this.bonificacion=bonificacion;
    }
    // set
    public void setCantVentas(Integer ventas)
    {
        this.cantidadVentas=ventas;
    }
    // get
    public Integer getCantVentas(){
        return cantidadVentas;
    }
    public Double getSalarioComision(){
        return salarioComisionado;
    }
    public Double getMonto(){
        return calcularSueldo();
    }
    // comportamientos
    public Double calcularSueldo(){
        if((cantidadVentas!=0)&&(bonificacion!=0))
            this.sueldo=(salarioComisionado*cantidadVentas)*bonificacion;
        else
            this.sueldo=salarioComisionado;
        return sueldo;
    }
}
