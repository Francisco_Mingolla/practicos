package root;

import java.util.ArrayList;
import java.util.Scanner;


public class Liquidaciones {
    private Scanner entrada;
    private Contrataciones contrataciones;
    // constructor
    public Liquidaciones(Contrataciones contrataciones){
        this.contrataciones=contrataciones;
    }
    // comportamientos
    public void pagarEmpleado() throws EmpleadoInexistenteException
    {
        Empleado empleadoEncontrado=null;
        int respuesta;
        Integer dni;
        entrada = new Scanner(System.in);
        System.out.print("Documento del empleado : ");
        dni=entrada.nextInt();
        empleadoEncontrado=contrataciones.buscarEmpleadoDni(dni);
        if(empleadoEncontrado!=null)
        {
            empleadoEncontrado.mostrarDatos();
            System.out.println("Desea pagar y cambiar el estado de " +empleadoEncontrado.getEstadoCobro());
            System.out.println("1: Si");
            System.out.println("2: No");
            System.out.print("Respuesta numerica: ");
            respuesta=entrada.nextInt();
            switch (respuesta) {
                case 1:
                    empleadoEncontrado=cambiarEstadoEmpleado(empleadoEncontrado, empleadoEncontrado.getEstado());
                    break;
                case 2:
                    break;
                default:
                    System.out.println("Respuesta incorrecta");
                    break;
            }
        }   
    }
    public Empleado cambiarEstadoEmpleado(Empleado empleado, boolean estadoDeCobro){
        if(estadoDeCobro)
            empleado.setEstado(false);
        else
            empleado.setEstado(true);
        return empleado;
    }
    public void pagarServicio() throws ServicioInexistenteException
    {
        Servicio servicioEncontrado=null;
        int respuesta;
        Integer cuit;
        entrada = new Scanner(System.in);
        System.out.print("Cuit del servicio : ");
        cuit=entrada.nextInt();
        servicioEncontrado=contrataciones.buscarServicioCuit(cuit);
        if(servicioEncontrado!=null)
        {
            servicioEncontrado.mostrarDatos();
            System.out.println("Desea pagar y cambiar el estado de " +servicioEncontrado.getEstadoCobro());
            System.out.println("1: Si");
            System.out.println("2: No");
            System.out.print("Respuesta numerica: ");
            respuesta=entrada.nextInt();
            switch (respuesta) {
                case 1:
                    servicioEncontrado=cambiarEstadoServicio(servicioEncontrado, servicioEncontrado.getEstado());
                    break;
                case 2:
                    break;
                default:
                    System.out.println("Respuesta incorrecta");
                    break;
            }
        }   
    }
    public Servicio cambiarEstadoServicio(Servicio servicio, boolean estadoDePago){
        if(estadoDePago)
            servicio.setEstado(false);
        else
            servicio.setEstado(true);
        return servicio;
    }
    //lista de empleados comisionados
    public ArrayList<Empleado> getListaComisionados(){
        ArrayList<Empleado> empleadosComisionados = new ArrayList<>();
        for (Empleado empleado : contrataciones.getListaEmpleados()) {
            if(empleado instanceof EmpleadoComisionado)
                empleadosComisionados.add(empleado);
        }
        return empleadosComisionados;
    }
    public void mostrarListaComisionados(){
        for (Empleado empleado : getListaComisionados()) {
            empleado.mostrarDatos();
        }
    }
    // agregar ventas empleado comisionado
    public void agregarVentasEmpComisionado() throws EmpleadoInexistenteException, EmpleadoNoComisionadoException{
        Empleado empleadoEncontrado=null;
        entrada = new Scanner(System.in);
        Integer dni,cantidadVentas;
        System.out.println("Ingrese el Documento del empleado, luego asigne su cantidad de ventas");
        mostrarListaComisionados();
        System.out.print("Documento: ");
        dni=entrada.nextInt();
        empleadoEncontrado=contrataciones.buscarEmpleadoDni(dni);
        if((empleadoEncontrado!=null))
        {
            if(empleadoEncontrado instanceof EmpleadoComisionado)
            {
                System.out.println("Empleado "+empleadoEncontrado.getNombre()+" "+empleadoEncontrado.getDni());
                System.out.print("Ventas: ");
                cantidadVentas=entrada.nextInt();
                setVentasComisionado((EmpleadoComisionado)empleadoEncontrado,cantidadVentas);
            }
            else
                throw new EmpleadoNoComisionadoException();
        }

    }
    public Integer setVentasComisionado(EmpleadoComisionado empleado,Integer cantidadVentas){
        empleado.setCantVentas(cantidadVentas);
        return empleado.getCantVentas();
    }
}
