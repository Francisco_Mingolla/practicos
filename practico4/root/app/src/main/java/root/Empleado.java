package root;

public abstract class Empleado implements InterfazEyS{
    // atributos
    private String nombre;
    private Integer dni;
    private String tipoEmpleado;
    private boolean estadoDeCobro;
    // constructor
    public Empleado(String nombre, Integer dni, String tipoEmpleado, boolean estadoDeCobro){
        this.nombre=nombre;
        this.dni=dni;
        this.tipoEmpleado=tipoEmpleado;
        this.estadoDeCobro=estadoDeCobro;
    }
    // set
    public void setEstado(boolean estado_de_Cobro)
    {
        this.estadoDeCobro=estado_de_Cobro;
    }
    // get
    public boolean getEstado()
    {
        return estadoDeCobro;
    }
    public String getEstadoCobro(){
        String estado;
        if(getEstado())
            estado="Pagado";
        else
            estado="Falta pagar";
        return estado;
    }
    public Integer getDni(){
        return dni;
    }
    public String getNombre(){
        return nombre;
    }
    public String getTipoEmpleado(){
        return tipoEmpleado;
    }
    // comportamientos
    public void mostrarDatos()
    {
        System.out.println("Nombre: " + getNombre());
        System.out.println("Tipo de empleado: " + getTipoEmpleado());
        System.out.println("Documento: " + getDni());
        System.out.println("Sueldo: " + getMonto());
        System.out.println("Estado del empleado : " + getEstadoCobro());
    }
}
