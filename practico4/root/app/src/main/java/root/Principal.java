package root;
import java.io.IOException;
import java.util.*;

// falta revisar

public class Principal
{
    public String getGreeting() {
        return "Bienvenido!";
    }
    public static void main(String[] args) throws IOException, EmpleadoInexistenteException, EmpleadoExistenteExcepcion, ServicioInexistenteException, ServicioExistenteExcepcion, EmpleadoNoComisionadoException
    {
        System.out.println(new Principal().getGreeting());
        // atributos
        int respuesta;
        Scanner entrada = new Scanner(System.in);
        ArrayList<Empleado> lista_Empleados = new ArrayList<Empleado>();
        ArrayList<Servicio> lista_Servicios = new ArrayList<Servicio>();
        Contrataciones contrataciones = new Contrataciones(lista_Empleados, lista_Servicios);
        Liquidaciones liquidacion = new Liquidaciones(contrataciones);
        // funcionamiento
        do
        {
            menuEmpresa();
            respuesta=entrada.nextInt();
            System.out.println("");
            switch (respuesta)
            {
                case 1:
                    if(contrataciones.comprobarSueldos()){
                        System.out.println("Los sueldos ya fueron asignados"); 
                        System.out.println("Desea editar los sueldos ? "); 
                        System.out.println("1: Si       2: No");
                        System.out.print("Respuesta numerica: ");
                        respuesta=entrada.nextInt();
                        switch (respuesta) {
                            case 1:
                                contrataciones.cargarSueldos();  
                                break;
                            case 2:
                                break;
                            default:
                                System.out.println("Respuesta incorrecta");
                            break;
                        }
                    }
                    else
                        contrataciones.cargarSueldos();    
                    break;
                case 2:
                    if(contrataciones.comprobarSueldos())
                        contrataciones.mostrarSueldosyBonificacion();
                    else
                        System.out.println("Sueldos no asignados");
                    break;
                case 3:
                    if(contrataciones.comprobarSueldos())
                        contrataciones.cargarEmpleado();
                    else
                    {
                        System.out.println("Antes de cargar un empleado debe asignar su sueldo");
                        contrataciones.cargarSueldos(); 
                    }
                    break;
                case 4:
                    if(contrataciones.getListaEmpleados().size()>0)
                        contrataciones.mostrarEmpleados();
                    else
                        System.out.println("Lista de empleados vacia");
                    break;
                case 5:
                    if(liquidacion.getListaComisionados().size()>0)
                        liquidacion.agregarVentasEmpComisionado();
                    else
                        System.out.println("Lista de empleados comisionados vacia");
                    break;
                case 6: 
                    contrataciones.cargarServicio();
                    break;
                case 7:
                    if(contrataciones.getListaServicio().size()>0)
                        contrataciones.mostrarServicios();
                    else
                        System.out.println("Lista de servicios vacia");
                    break;
                case 8:
                    if(contrataciones.getListaEmpleados().size()>0){
                        contrataciones.mostrarEmpleados();
                        liquidacion.pagarEmpleado();
                    }
                        else
                        System.out.println("Lista de empleados vacia");
                    break;
                case 9:
                    if(contrataciones.getListaServicio().size()>0){
                        contrataciones.mostrarServicios();
                        liquidacion.pagarServicio();
                    }
                    else
                        System.out.println("Lista de servicios vacia");
                    break;
                case 10:
                    if(contrataciones.getListaEmpleados().size()>0){

                        contrataciones.mostrarEmpleados();
                        contrataciones.buscar_eliminarEmpleado();
                    }
                    else
                        System.out.println("Lista de Empleados vacia");
                    break;
                case 11:
                    if(contrataciones.getListaServicio().size()>0){
                        contrataciones.mostrarServicios();
                        contrataciones.buscar_eliminarServicio();
                    }
                    else
                        System.out.println("Lista de servicios vacia");
                    break;
                case 12:
                    break;    
                default:
                    System.out.println("Respuesta incorrecta");
                    break;
            }
        }while(respuesta!=12);
        System.out.println("Salio del algoritmo");
    }
    //Metodos
    private static void menuEmpresa()
    {
        System.out.println("");
        System.out.println("Menu: ");
        System.out.println("1: Asignar sueldo y bonificaciones a los empleados");
        System.out.println("2: Mostrar sueldos de los empleados");
        System.out.println("3: Cargar Empleado");
        System.out.println("4: Mostrar Empleados");
        System.out.println("5: Asignar ventas a empleados comisionados");
        System.out.println("6: Cargar Servicio");
        System.out.println("7: Mostrar Servicios");
        System.out.println("8: Pagar Empleados");
        System.out.println("9: Pagar Servicios");
        System.out.println("10: Eliminar Empleado");
        System.out.println("11: Eliminar Servicio");
        System.out.println("12: Salir");
        System.out.print("Respuesta: ");
    }
}