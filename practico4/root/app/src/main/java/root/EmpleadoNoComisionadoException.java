package root;

public class EmpleadoNoComisionadoException extends RuntimeException{
    public EmpleadoNoComisionadoException(){
        super("El empleado seleccionado No es Comisionado");
    }
}
