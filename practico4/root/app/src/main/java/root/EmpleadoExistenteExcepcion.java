package root;

public class EmpleadoExistenteExcepcion extends RuntimeException{
    public EmpleadoExistenteExcepcion(){
        super("El empleado que quiere agregar ya existe");
    }
}