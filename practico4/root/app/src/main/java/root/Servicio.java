package root;

public class Servicio implements InterfazEyS{
    // atributos
    private String nombre;
    private Integer cuit;
    private double monto;
    private boolean estadoDeCobro;
    // constructor
    public Servicio(String nombre, Integer cuit, double monto){
        this.nombre = nombre;
        this.cuit=cuit;
        this.monto=monto;
        this.estadoDeCobro=false;
    }
    // pagar servicio
    public boolean setPagarServicio(double pago){
        if(pago==monto){
            setEstado(true);
        }
        return estadoDeCobro;
    }
    // set estado servicio
    public void setEstado(boolean estado_de_Cobro)
    {
        this.estadoDeCobro=estado_de_Cobro;
    }
    // get
    public boolean getEstado(){
        return estadoDeCobro;
    }
    public String getEstadoCobro(){
        String estado;
        if(getEstado())
            estado="Pagado";
        else
            estado="Falta pagar";
        return estado;
    }
    public String getNombre(){
        return nombre;
    }
    public Integer getCuit(){
        return cuit;
    }
    public Double getMonto(){
        return monto;
    }
    public void mostrarDatos()
    {
        System.out.println("Nombre: " + getNombre());
        System.out.println("Cuit: " + getCuit());
        System.out.println("Monto del servicio:  " + getMonto());
        System.out.println("Estado del servicio : " + getEstadoCobro());
    }
}
