package root;

public class Producto {
    // atributos
    private String nombre;
    private int cantidad;
    private double precioUnidad;
    private double precioTotal;
    public Producto(){
        this.nombre = "0";
        this.cantidad = 0;
        this.precioUnidad = 0;
        this.precioTotal = 0;
    }
    // metodos
    public void setNombre(String n)
    {
        this.nombre = n;
    }
    public void setCantidad(int n)
    {
        this.cantidad = n;
    }
    public void setPrecioUnidad(double n)
    {
        this.precioUnidad = n;
        this.precioTotal = cantidad * precioUnidad;
    }
    public double getPrecioTotalProducto()
    {
        return precioTotal;
    }
    public String getNombre(){
        return nombre;
    }
    public int getCantidad(){
        return cantidad;
    }
    public double getPrecioUnidad(){
        return precioUnidad;
    }
}
