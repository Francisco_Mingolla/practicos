package root;
import java.io.IOException;
import java.util.*;

public class Principal 
{
    public static void main(String[] args) throws IOException
    {
        int respuesta;
        Pedido pedido = null;
        ArrayList<Pedido> lista = new ArrayList<Pedido>();
        Scanner entrada = new Scanner(System.in);
        do
        {
            menu();
            respuesta=entrada.nextInt();
            switch (respuesta)
            {
                case 1:
                    pedido = new Pedido();
                    lista = cargarPedido(pedido, lista);
                    break;
                case 2:
                    if(pedido != null)
                    {
                        cambiarEstado(lista);
                    }
                    else
                    {
                        System.out.println("");
                        System.out.println("No se encontraron pedidos");
                    }
                    break;
                case 3:
                    if(pedido != null)
                    {
                        mostrar_pedidos(lista);
                    }
                    else
                    {
                        System.out.println("");
                        System.out.println("No se encontraron pedidos");
                    }
                    break;

                case 4:
                    break;
                default:
                    System.out.println("Respuesta incorrecta");
                    break;
            }
        }while(respuesta!=4);
        System.out.println("Salio del algoritmo");
    }
    //Metodos
    private static void menu()
    {
        System.out.println("");
        System.out.println("Sistema de manejos de 'Pedidos'");
        System.out.println("Menu:");
        System.out.println("1: Agregar");
        System.out.println("2: Cambiar el estado del pedido");
        System.out.println("3: Mostrar pedidos");
        System.out.println("4: Salir");
        System.out.print("Respuesta numerica: ");
    }
    private static void menu_pedido()
    {
        System.out.println("----------------------- Menu del Pedido -----------------------");
        System.out.println("1: Agregar producto");
        System.out.println("2: Terminar pedido - pasara a estar pendiente de cobro");
        System.out.print("Respuesta numerica: ");
    }
    private static void menu_estados(){
        System.out.println("----------------------- Estados del pedido -----------------------");
        System.out.println("1: Pagado");
        System.out.println("2: Procesando");
        System.out.println("3: Enviado");
        System.out.println("4: Entregado");
        System.out.print("Respuesta numerica: ");
    }
    //---------------------------------------------------------
    private static ArrayList<Pedido> cargarPedido(Pedido pedido,ArrayList<Pedido> lista) throws IOException
    {
        Scanner entrada = new Scanner(System.in);
        int respuesta;
        System.out.println("Complete la descripcion del Pedido");
        System.out.print("Numero: ");
        pedido.setNro(entrada.nextInt());
        entrada.nextLine();
        System.out.print("Direccion: ");
        pedido.setDireccion(entrada.nextLine());
        do {
            menu_pedido();
            respuesta = entrada.nextInt();
            switch (respuesta)
            {
                case 1:
                    pedido.agregarProducto();
                    break;
                case 2:
                    pedido.setEstado("Pendiente");
                    lista.add(pedido);
                    System.out.println("");
                    System.out.println("Pedido agregado");
                    break;
                default:
                    System.out.println("Respuesta incorrecta");
                    break;
            }
        } while (respuesta!=2);
        return lista;
    }
    //---------------------------------------------------------
    private static void mostrar_pedidos(ArrayList<Pedido> lista)
    {
        for (Pedido pedido : lista)
        {
            System.out.println("-------------------------------------------------------");
            pedido.mostrarPedido();
        }
    }
    //---------------------------------------------------------
    public static void cambiarEstado(ArrayList<Pedido> lista)
    {
        int respuesta;
        boolean comprobarPedido = false, comprobarEstado = false;
        Scanner entrada = new Scanner(System.in);
        System.out.println("");
        System.out.println("Pedidos");
        mostrar_pedidos(lista);
        System.out.println("");
        System.out.print("Numero del pedido a cambiar: ");
        respuesta = entrada.nextInt();
        for (Pedido pedido : lista)
        {
            if(respuesta == pedido.getNumero())
            {
                System.out.println("El pedido con numero: " + respuesta + " Cambiara su estado");
                menu_estados();
                respuesta = entrada.nextInt();
                switch (respuesta)
                {
                    case 1:
                        pedido.setEstado("Pagado");
                        comprobarEstado = true;
                        break;
                    case 2:
                        pedido.setEstado("Procesando");
                        comprobarEstado = true;
                        break;
                    case 3:
                        pedido.setEstado("Enviado");
                        comprobarEstado = true;
                        break;
                    case 4:
                        pedido.setEstado("Entregado");
                        comprobarEstado = true;
                    default:
                        System.out.println("");
                        System.out.println("Respuesta incorrecta");
                        break;
                }
                comprobarPedido = true;
            }
        }
        if(comprobarPedido)
        {
            if(comprobarEstado)
            {
                System.out.println("");
                System.out.println("Estado del pedido actualizado");
            }
            else{
                System.out.println("Pedido encontrado, pero no pudo ser actualizado");
            }
        }
        else
        {
            System.out.println("");
            System.out.println("Pedido no encontrado");
        }
    }
}