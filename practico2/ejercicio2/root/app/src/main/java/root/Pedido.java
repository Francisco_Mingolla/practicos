package root;
import java.util.ArrayList;
import java.util.Scanner;

public class Pedido {
    // atributos
    private int nro;
    private String direccion;
    private String estado;
    Producto producto;
    ArrayList<Producto> lista;
    private double precio;
    // constructor
    public Pedido(){
        this.nro = 0;
        this.direccion = "O";
        this.producto = null;
        this.lista = new ArrayList<Producto>();
        this.precio = 0;
        this.estado = "O";
    }
    // metodos
    public void setNro(int n)
    {
        this.nro=n;
    }
    public void setDireccion(String n)
    {
        this.direccion=n;
    }
    public void setEstado(String n)
    {
        this.estado=n;
    }
    public void setProducto(Producto n)
    {
        this.lista.add(n);
    }
    public int getNumero()
    {
        return nro;
    }
    // -----------------------------------------------------------------------
    public void agregarProducto()
    {
        Scanner entrada = new Scanner(System.in);
        int respuesta;
        producto = new Producto();
        System.out.println("Complete la descripcion del producto");
        System.out.print("Nombre: ");
        producto.setNombre(entrada.nextLine());
        System.out.print("Cantidad: ");
        producto.setCantidad(entrada.nextInt());
        System.out.print("Precio: ");
        producto.setPrecioUnidad(entrada.nextDouble());
        precio += producto.getPrecioTotalProducto();
        setProducto(producto);
    }
    // -----------------------------------------------------------------------
    public void mostrarPedido(){
        System.out.println("");    
        System.out.println("Numero del pedido: " + nro);
        System.out.println("Direccion del pedido: " + direccion);
        System.out.println("Estado del pedido: " + estado);
        System.out.println("Productos:");
        for (Producto producto : lista)
        {
            
            System.out.println("Nombre: " + producto.getNombre());
            System.out.println("Cantidad: " + producto.getCantidad());
            System.out.println("Precio por unidad: " + producto.getPrecioUnidad() + "     Precio Total: " + producto.getPrecioTotalProducto());
        }
        System.out.println(" ****** Precio total del pedido: " + precio + " ****** ");
    }
}
