package root;

public class Entidad {
    // atributos
    private String nombre;
    private String tipo;
    private double salario;
    private boolean cobro;
    // constructor
    public Entidad()
    {
        this.nombre="O";
        this.tipo="O";
        this.salario=0;
        this.cobro=false;
    }
    // sets
    public void setNombre(String n)
    {
        this.nombre=n;
    }
    public void setTipo(String n)
    {
        this.tipo=n;
    }
    public void setSalario(double n)
    {
        this.salario=n;
    }
    public void setCobro(boolean n)
    {
        this.cobro=n;
    }
    // gets
    public String getNombre()
    {
        return nombre;
    }
    public String getTipo()
    {
        return tipo;
    }
    public double getSalario()
    {
        return salario;
    }
    public boolean getCobro()
    {
        return cobro;
    }
    // metodo
    public void mostrar_entidad()
    {
        System.out.println("Nombre: " + nombre);
        System.out.println("Tipo: " + tipo);
        System.out.println("Salario: " + salario);
        System.out.println("Cobro:  " + cobro);
    }
}
