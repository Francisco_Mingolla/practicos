package root;
import java.io.IOException;
import java.util.*;
public class Principal 
{
    public static void main(String[] args) throws IOException
    {
        int respuesta;
        Entidad entidad = null;
        ArrayList<Entidad> lista = new ArrayList<Entidad>();
        Scanner entrada = new Scanner(System.in);
        do
        {
            menu();
            respuesta=entrada.nextInt();
            switch (respuesta)
            {
                case 1:
                    entidad = new Entidad();
                    lista=cargar_entidad(entidad,lista);
                    break;
                case 2:
                    if(entidad != null)
                    {
                        mostrar_entidades(lista);
                    }
                    else
                    {
                        System.out.println("No se encontraron entidades");
                    }
                    break;
                case 3:
                    if(entidad != null)
                    {
                        pagar(lista);
                    }
                    else
                    {
                        System.out.println("No se encontraron entidades");
                    }
                    break;
                case 4:
                    break;
                default:
                    System.out.println("Respuesta incorrecta");
                    break;
            }
        }while(respuesta!=4);
        System.out.println("Salio del algoritmo");
    }
    //Metodos
    private static void menu()
    {
        System.out.println("");
        System.out.println("Menu: ");
        System.out.println("1: Cargar Entidad");
        System.out.println("2: Mostrar Entidades");
        System.out.println("3: Pagar");
        System.out.println("4: Salir");
        System.out.print("Respuesta: ");
    }
    //
    private static ArrayList<Entidad> cargar_entidad(Entidad entidad, ArrayList<Entidad> lista) throws IOException
    {
        Scanner entrada = new Scanner(System.in);
        System.out.println("Complete la descripcion del Empleado");
        System.out.print("Nombre: ");
        entidad.setNombre(entrada.nextLine());
        System.out.print("Tipo: ");
        entidad.setTipo(entrada.nextLine());
        System.out.print("Salario: ");
        entidad.setSalario(entrada.nextDouble());
        if(lista.add(entidad))
        {
            System.out.println("Entidad cargada");
        }
        else
        {
            System.out.println("Error al cargar entidad");
        }
        return lista;
    }
    //---------------------------------------------------------
    private static void mostrar_entidades(ArrayList<Entidad> lista)
    {
        int contador=1;
        for (Entidad _entidad_ : lista)
        {
            System.out.println("");
            System.out.println("Entidad Numero: "+ contador);
            _entidad_.mostrar_entidad();
            contador++;
        }
    }
    private static void pagar(ArrayList<Entidad> lista)
    {
        Scanner entrada = new Scanner(System.in);
        int nro=0;
        int contador=1;
        mostrar_entidades(lista);
        System.out.println("");
        System.out.print("Seleccione el nro de la entidad que quiere pagarle : ");
        nro=entrada.nextInt();
        if(nro>0 && nro<=lista.size())
        {
            for (Entidad _entidad_ : lista)
            {
                if(contador==nro)
                {
                    if(_entidad_.getCobro())
                    {
                        System.out.println("Esta entidad ya fue pagada");
                    }
                    else
                    {
                        _entidad_.setCobro(true);
                        System.out.println("Entidad pagada");
                    }
                }
                contador++;
            }
        }
    }
}